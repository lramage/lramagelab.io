---
layout: post
title: Jazzy
date: 2017-09-29 09:25:45
categories: robotics microcontroller linux jazzy
---

I've been extraordinarily busy with college the last few weeks. With family and work and school, there has not been very much time left for tinkering. 
However, I did have a some free time last night and I did some research for my robotics platform. I'm building an autonomous platform out of an electric wheelchair base. 
The model I have is from the early nineties. I wouldn't be surprised if it's the same age as me. I managed to disassemble the controller and found embedded inside, a H8/532 microcontroller. 
Specifically, it is a HD6475328CP10. I read up on the [H8 family of microcontrollers](https://en.m.wikipedia.org/wiki/H8_Family) and learned that some of the models are capable of running Linux! 
I would need to purchase a [programming adaptor]( http://www.mqp.com/ad64.htm) to even begin messing with it.

Also, I've been working on my Amazon fire phone kernel.

- [Gentoo Wiki - Amazon Fire Phone](https://wiki.gentoo.org/wiki/User:Rage/Drafts/Amazon_Fire_Phone_(Kodiak))

A handy trick I figured out for working with quilt and my local overlay,

```
    $ emerge sys-kernel/kodiak-sources dev-util/quilt
    $ cd /usr/src/kodiak-sources-4.6.6/ && \
    > ln -s /usr/local/portage/rage-overlay/sys-kernel/kodiak-sources/files/ patches
    $ quilt new 9999-misc_fixes.patch
```

Then before working on a specific file, run `quilt add <filename>`, then
Once you are finished, run `quilt refresh` to update your patch.

- [Die.net - Quilt](https://linux.die.net/man/1/quilt)
- [Gentoo Wiki - Ebuild Patches](https://devmanual.gentoo.org/ebuild-writing/misc-files/patches/index.html)
