---
layout: post
title: Dumping data from SQLite using the CLI
date: '2016-11-28T07:36:17-05:00'
---
First we will open up our database.

sqlite3 .db

Then set the mode to CSV and turn on our headers.

.mode csv
.headers on

Then we will set the output file.

.out .dmp

And finally, we will get our data to export.

SELECT * FROM <table_name>;</table_name>

If we exit sqlite, we will see our output file in the current working directory. We can then cat out the file and see the data from our table.
