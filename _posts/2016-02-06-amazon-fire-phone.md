---
layout: post
title: Amazon Fire Phone
date: '2016-02-06T12:16:31-05:00'
---
The Amazon Fire Phone was pretty much a disaster for Amazon. It was hyped up pretty big but it was just too expensive and fire os had major compatibility issues with regular android apps. The hardware is very nice though. I got my wife a fire phone for $80 bucks on Newegg after her stupid iPhone 4S finally gave up on life. I really liked it when she first got it. The camera is nice, the 3D effects were interesting, but Facebook never worked properly, there was no Google Apps or even good alternatives to them. It just seemed lacking. But that’s when Cyanogenmod came to the rescue. Take an almost $500 phone, with crappy software and flash a custom rom and you have yourself a nice phone. The only version of CM out there for the Fire is 11 so it is kind of old but it works very well.
