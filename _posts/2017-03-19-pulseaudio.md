---
layout: post
title: Pulse Audio
date: 2017-03-19 3:36:21
categories: log arch linux chroot gaming jekyll
---

Here is a quick update on my arch linux gaming chroot.

So I was having audio issues in my chroot and I had been searching the web and I was finding a lot of information about setting up pulseaudio and making sure I have alsa-plugins
but I already had all that and it still wasn't working.
If I ran, sudo aplay -l I could see my graphics cards but I could not access them as lramage in my chroot even though I was in the audio group. Then I did something nasty. 
I opened up /etc/group and changed my group ids to match my host. I have no idea what that will do to me later on and if anything, this whole experience has only reinforced
the importance of containers in my mind. There are just so many different pieces to an operating system and while a chroot seems easier and simpler at first, it can only lead
to a path of pain. Unfortunately I am a masochist and enjoy traveling [the road not taken](https://www.poetryfoundation.org/resources/learning/core-poems/detail/44272 "Robert Frost").

Also, I am beginning to form habits as I am writing this blog and thus I am looking to simplify my work flow by automation. I need to go back and recategorize all of my old posts
so that I can start filtering my posts with my [tag cloud](http://www.goat1000.com/tagcanvas.php). Right now it is rather [ornamental](http://dictionary.cambridge.org/dictionary/english/ornamental?a=british). I am going to start researching some vim macros for markdown to simplify inserting hyperlinks. 

Here are a few resources that I found,

- [Wiki Static Generators](https://gist.github.com/dypsilon/4552696])
