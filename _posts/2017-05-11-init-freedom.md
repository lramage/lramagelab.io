---
layout: post
title: "Init Freedom"
date: 2017-05-11 07:15:45
categories: linux init freedom
---

Today I tried [Void Linux](http://www.voidlinux.eu/), as part of my
continuing search for a good, systemd-free, out of the box distribution for work.

As someone who enjoys tinkering with linux, I am not a fan of systemd.
I have been researching replacements for Arch Linux, and Debian. Those
were my main distributions of choice until recently. I do not understand
how systemd and the Arch Linux philosophy are even compatible. I also
dislike that they are dropping support for 32bit machines. I have quite
a collection of older machines that I used to run Arch on. And yes, I know
that there will still be community support. Suffice to say, 
I have fallen out of love with that distribution. The Arch wiki, however,
is still very high on my list when searching for documentation.

It is my hope that [Devuan](https://devuan.org) reaches maturity, and if it does,
then I will likely use that for my work laptop. Gentoo has become my favorite, 
but I simply cannot take the time to run it on my work laptop.
I also really admire Alpine Linux, but I do not think I want to use it for my my daily driver.

Here are some interesting resources on the topic:
- [A history of modern init systems (1992-2015)](http://blog.darknedgy.net/technology/2015/09/05/0/)
- [Suckless - Systemd](http://suckless.org/sucks/systemd)
- [Thoughts on Systemd](https://fitzcarraldoblog.wordpress.com/2014/10/04/my-thoughts-on-systemd/)
- [Without Systemd](http://without-systemd.org/wiki/index.php/Main_Page)
