---
layout: post
title: "Towards the perfect ruleset"
date: 2017-03-31 9:27:02
categories: linux networking iptables programming
---

I was browsing the internet looking for more quality documentation on
iptables and I stumbled across this paper entitled "Towards the perfect ruleset" by
Jan Engelhardt. It is a great resource for setting up a firewall in Linux.

[Towards the perfect ruleset](http://inai.de/documents/Perfect_Ruleset.pdf)
