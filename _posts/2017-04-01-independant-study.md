---
layout: post
title: "Independent Study"
date: 2017-04-01 4:38:11
categories: science progress mathematics
---

Today I went to one of my favourite places, the library, and picked up many books on
mathematics and physics. One of such books that I began reading is [Einstein's Theory of Relativity by Max Born](http://store.doverpublications.com/0486607690.html).
I only read the first chapter on "Geometry and Cosmology" and part of the second chapter "The Fundamental Laws of Classical Mechanics" (up to page 35), and 
I must admit that I am having a hard time comprehending the concepts. But then I am reminded of Proverbs 13:20, "He that walketh with wise men shall be wise..",
and so I must perservere so that I may overcome my foolishness. I also grabbed quite a few other books which I will post shortly.
