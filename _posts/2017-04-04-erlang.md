---
layout: post
title: "Erlang"
date: 2017-04-04 2:54:43
categories: erlang multi-paradigm programming
---

Over the past few days I have been cutting my teeth on erlang for an up and coming work opportunity. I enjoy being
challenged and learning new things while at work. Money will come and go, but knowledge and wisdom will stay with you
for a very long time. It's definitely an interesting language with a rather strange syntax compared to other programming
languages that I have used.

- [Learn You Some Erlang](http://learnyousomeerlang.com/content)
