---
layout: post
title:  "Gentoo Rap"
date:   2017-03-09 10:09:31
categories: gentoo rap android fire phone custom kernel xda jekyll lakka pi gentoo linux drivers automation lxc
---

This morning I had a lovely meeting on google hangouts with some of the gentoo developers. 
I asked about contributing to the [Gentoo RAP Project](https://wiki.gentoo.org/wiki/Project:Android#Gentoo_RAP)
and I think I have a pretty good idea of what I can do to help. For my [fire phone](https://www.amazon.com/dp/B00EOE0WKQ "Amazon Fire Phone"),
The first step will be to prepare the kernel for a glibc system. They recommended SailfishOS kernel configs as a good starting point
to reference. I also found [this post](https://forum.xda-developers.com/fire-phone/development/dev-building-custom-kernel-kernel-t3195492 "Custom Kernel") 
on the xda developer forums about building a stock kernel using Amazon's source code.

I have also begun refining some of my website designs. I like jekyll and some of the concepts that it uses,
but I feel that I could do more with less as I prefer the minimalism. I would also like to have a no-js
website. I'm beginning to narrow in on the features that I want- markdown editing -> html, base16 dark minimal theme,
use of the GNU Autotools, live updating as I write (invoked via make watch), and pretty permalinks.

Also, I finally got around to flashing [Lakka](http://www.lakka.tv/ "Lakka") on my old raspberry pi 2 model b.
I love the raspberry pi as much as the next guy, but it just seems very slow compared to using my cellphone.
I'm assuming that's because of the sdcard read/write access? I have started collecting old phones from people
for development and for dissassembling. I even have quite a few old laptops that run emulators better.
That being said, if I can find the time, I would like to begin working on debugging front mission 3 for 
[playstation emulation](https://github.com/lramage94/pcsx_rearmed "pcsx_rearmed"). 
It crashes quite frequently on both my phone and on various computers.

I also spent a longer than I should have on compiling the drivers for a [Realtek RTL8191SU](http://www.realtek.com/products/productsView.aspx?Langid=1&PFid=48&Level=5&Conn=4&ProdID=229 "RTL8191SU") USB Wifi Adapter. I read through [this forum post](https://forums.gentoo.org/viewtopic-p-7025566.html "Gentoo Forums")
and finally figured it out.

And finally, I have fired up gentoo my recently aquired [Dell Poweredge 2900](http://www.dell.com/us/dfb/p/poweredge-2900/pd "PE2900")
on which I plan to start learning about lxc containerization. I am going to put ubuntu in a container first so that I can build
ubuntu touch images. I'd also like to setup build automation tools, particularly [Buildbot](http://buildbot.net/ "buildbot") so that
I can start automating some of my kernel development.
