---
layout: post
title: "Mechanophobia"
date: 2017-03-21 8:18:27
categories: mechanophobia log cherry picking git automation grok erlang steam bug
---

I missed the opporunity to post my daily progress yesterday do to a car accident so I will briefly summarize.

- Started a micro blog for work. It's a private repo because I don't think my job will allow me to share it's contents.
I am using it to keep track of my daily conundrums for the purpose of abstracting some of my experiences for my own personal reference.
- Fixed the mwan3 failover issue that was plaguing me for the last two weeks! It was a delicious little hack but I started refining it
today. (I might create a gist of it on github at some point in the future.)

Now for today, I found [an article](http://www.draconianoverlord.com/2013/09/07/no-cherry-picking.html)
on a software development blog this morning about setting up a git project and branching. 
And while I found it very insightful, 
it also referenced this [git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
which I had never seen before. It amazes me how there is so much knowledge on the internet.

Now I see the near future of automation as an exciting time to be alive. I feel as though we
are right on the brink of the next big technical revolution. Although, there are times that I
think I am the only one with a positive outlook. Maybe my optimism stems from a lack of
social and political experiences that are a by-product of youth. Whatever the case, I read an article
today entitled ["On Our Ingrained Mechanophobia"](http://omarrr.com/on-our-ingrained-mechanophobia/)
and I believe that he and I are in accord on the topic. It is definitely an interesting read.

I keep seeing the term "Grokked" in various places around the web in the context of "I grok <insert programming language here>",
and I was curious as to it's etymology. Apparently, it is a word invented by Robert A. Heinlein back in the sixties.
You can find more information on [Wikipedia](https://en.wikipedia.org/wiki/Grok).

I plan on finishing out my evening by learning a bit of Erlang. My good friend [Trevor](http://stratus3d.com/) is quite the
erlang expert (as well as ruby among other languages) and he pointed me towards [Learn X in Y minutes](https://learnxinyminutes.com/docs/erlang/),
as well as [Try Erlang](http://www.tryerlang.org/)- Both seem to be very good resources for beginners.

And lastly, I found [my bug](https://github.com/ValveSoftware/steam-for-linux/issues/2431) on the official steam-for-linux project on github!
