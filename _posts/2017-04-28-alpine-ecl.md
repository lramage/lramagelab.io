---
layout: post
title: "Compiling ECL on Alpine Linux"
date: 2017-04-28 9:51:03
categories: embedded common lisp alpine linux
---

I woke up this morning and checked my build and it was fine! Either I am getting better at this, or I am just lucky,
but it build in the first try. However, I have a new issue with ssh. When I get home I will update this post with the error message.
But until I fix that, I sadly cannot remote in and continue my work.

I learned something new yesterday about overlaytmpfs while setting up alpine that left me so stimulated that even now I am still recovering.
I am going to write a simple script so that I can setup a small persistent overlay for work on an openwrt device.
