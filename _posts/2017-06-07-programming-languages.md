---
layout: post
title: "Programming Language Adoption"
date: 2017-06-07 03:00:00
categories: programming languages adoption
---

I always wonder why certain languages and technologies become prominent even though there may be
technically superior languages to choose from. This article offers some valuable perspective on this
topic.

- [Outspeaking: The Lemon Market of Programming Language Adoption](http://outspeaking.com/words-of-technology/the-lemon-market-of-programming-language-adoption.html)
