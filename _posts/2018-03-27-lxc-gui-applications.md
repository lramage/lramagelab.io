---
layout: post
title: LXC Host X Server Sharing
date: 2018-03-27 02:27:15
categories: lxc linux containers xorg
---

To launch a desktop application from the host,

```
    $ function lxc-launch() {
    >   sudo lxc-attach --clear-env -n $CONTAINER -- sudo -u ubuntu -i env DISPLAY=$DISPLAY $@
    > }

    $ CONTAINER=ubuntu-dev lxc-launch firefox
```

To launch an application from inside an lxc terminal session.

```
    $ lxc-console -n $CONTAINER
    # login to your user account
    $ export DISPLAY=:0 # or whatever host $DISPLAY variable is set to.
    $ inkscape
````

References:

- [dotfiles/.local/etc/lxc/default.conf](https://github.com/lramage94/dotfiles/commit/f76cdc87883fea2b6d53eee3a6947938c25ccd4b)
- [Stephane Graber - LXC 1.0](https://stgraber.org/2013/12/20/lxc-1-0-blog-post-series/)
