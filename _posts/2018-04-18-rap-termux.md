---
layout: post
title: Gentoo RAP in Termux
date: 2018-04-18 11:51:42
categories: android linux gentoo rap
last_modified_at: 2018-05-11 9:20:28
---

Variables

    ACCEPT_KEYWORDS="~amd64"
    EPREFIX="/data/data/com.termux/files/home/.local/var/tmp/gentoo"
    PROFILE_PATH="/usr/portage/profiles/default/linux/arm/17.0/armv7a/prefix"

    LD_PRELOAD="${EPREFIX}/lib/libc.so.6"
    PATH="${PATH}:${EPREFIX}/usr/bin"
    $EPREFIX/usr/bin/gcc -v

Output from bootstrap-rap.sh

    Ok, I'm going to do a little bit of guesswork here.  Thing is, your
    machine appears to be identified by CHOST=armv7l-pc-linux-gnu

Termux Dependencies

    pkg install clang autoconf libtool

Installing Portage

    git clone https://github.com/gentoo/portage

References:

- [Gentoo Project:Android/build](https://wiki.gentoo.org/wiki/Project:Android/build)

- [Gentoo Wiki: Embedded Handbook - Cross-compiling with Portage](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Cross-compiling_with_Portage)

- [Termux Wiki: Package Management](https://wiki.termux.com/wiki/Package_Management)
