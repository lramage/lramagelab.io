---
layout: post
title: Formatting Date & Time in Linux
date: '2016-12-21T16:10:52-05:00'
---

By default, many linux programs from window managers such as dwm, i3. openbox, to desktop environments like XFCE, to desktop widgets like conky all seem to use a 24 Hour time format for the system clock. Until today, I always googled ‘How to set time format to 12 hour format’ and then found some forum thread, copy and pasted the information and then forgot about it until the next time I needed it. I’m sure many modern programmers will agree that Google, or Stack Overflow is their primary source of documenation. But recently I have been trying to dive into the linux man pages as my first source before going online so that I can get a deeper understanding of how things work.

For formatting date and time settings in linux, we can reference the strftime man page. It has a list of all of the options that we can set.

%H : %M :%S displays the hour, minute, and second in 24 Hour time format.

%A : %M :%S displays the same but using 12 Hour time format.

%k is similar to %H as it displays the hour using a 24 hour format,

and %l is similat to %A using a 12 hour format.

%m stands for month in decimal format i.e. 1 - 12.

%j stands for the day of the year as a decimal number i.e. 001 - 365.
