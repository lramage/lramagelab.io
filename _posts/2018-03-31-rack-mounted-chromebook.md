---
layout: post
title: Rack Mounted Chromebook
date: 2018-03-31 08:58:01
categories: linux hardware recycling hacking server rack mount chromebook
last_modified_at: 2018-04-07 01:26:21
---

The chromebook used for this project has had its screen broken for a couple years
and has been out of use ever since. The device was completely disassembled, leaving
both the motherboard and the battery to be mounted on standoffs in the chassis.
Also, ChromeOS was not acceptable for running headlessly, so I got Alpine Linux
running natively and installed an ssh-server for remote access.

I was able to configure Chrome OS via an external monitor. However, by default, 
external displays are set to extended mode, which makes it hard to change the
settings since the primary display is missing. The hotkey to toggle display 
mirroring is `Ctrl + F4`.

References:

- [Alpine Linux](https://alpinelinux.org)

- [Chromium OS Developer Guide](https://www.chromium.org/chromium-os/developer-guide)

- [Digi-Key 8" Rack Mount Chassis 1u](https://www.digikey.com/product-detail/en/hammond-manufacturing/RM1U1908SBK/HM995-ND/2094732)

- [Gentoo Embedded Handbook](https://wiki.gentoo.org/wiki/Embedded_Handbook)

- [Google Support - Connect your Chromebook to a monitor](https://support.google.com/chromebook/answer/1060909?hl=en)

- [Native Linux on Chromebook Guide](https://www.codedonut.com/chromebook/install-full-native-standalone-linux-on-any-chromebook-elementaryos)

- [Samsung Chromebook XE303C12](https://www.samsung.com/us/support/owners/product/chromebook-xe303c12)
