---
layout: post
title: Software Freedom in 2018
date: 2018-01-15 10:17:37
categories: freedom software open source
---

Web:

Blogging
- [jekyll](https://jekyllrb.com/)

Code
- [gitlab](https://gitlab.com/)

Email
- [openmailbox](https://www.openmailbox.org/)

Files
- [nextcloud](https://nextcloud.com/)

Productivity
- [sandstorm](https://sandstorm.io/)

Search
- [duckduckgo](https://duckduckgo.com/)

Social
- [mastodon](https://mastodon.social/)

Desktop:

Operating System
- [gnu+linux](https://www.getgnulinux.org)

Web browser
- [qutebrowser](https://qutebrowser.org/)

Window Manager:
- [dwm](https://dwm.suckless.org/)

Mobile:

Operating System
- [lineageos](https://lineageos.org/)

Launcher
- [kiss](https://github.com/Neamar/KISS)

Package Manager
- [fdroid](https://f-droid.org/)

Web browser
- [lightning](https://github.com/anthonycr/Lightning-Browser)
