---
layout: post
title: "Libray Books"
date: 2017-04-01 9:20:21
categories: library books education knowledge
---

Here is a list of items that I checked out at the library today,

- [Herrmann, Edward. Einstein: His Life and Universe](http://www.audible.com/pd/Bios-Memoirs/Einstein-Audiobook/B002V1A1YQ)
- [Thrower, Norman. Standing on the shoulders of Giants](http://www.ucpress.edu/op.php?isbn=9780520065895)
- [Bellos, Alex. The Grapes of Math: How Life Reflects Numbers and Numbers Reflect Life](http://www.simonandschuster.com/books/The-Grapes-of-Math/Alex-Bellos/9781451640113)
- [Numbers: Their Tales, Types, and Treasures](http://www.maa.org/press/maa-reviews/numbers-their-tales-types-and-treasures)
- [Born, Max. Einstein's Theory of Relativity](https://archive.org/details/einsteinstheoryo00born)
- [Kythe, P., Puri P.,i & Schaferkotter M. Partial Differential Equations and Mathematica](https://www.scribd.com/doc/43475600/Kythe-P-Partial-Differential-Equations-and-Ma-Thematic-A)
- [Cropper, William. Great Physicists: The Life and Times of Leading Physicists from Galileo to Hawking](https://archive.org/stream/GreatPhysicists/Great%20Physicists%20-%20From%20Galileo%20to%20Hawking%20-%20W.%20Cropper_djvu.txt)
- [Yizze, J. P., & Munem, M. A. Precalculus](https://mathed.byu.edu/~peterson/Homework/PreCalc%20Chapter%201.pdf)
