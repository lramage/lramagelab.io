---
layout: post
title: "Install Debian Over Serial Console"
date: 2017-04-05 11:53:06
categories: debian serial console installation linux embedded qemu
---

Today has been really exciting for me, for it was the first time that I have ever installed an operating
system over a serial console. Normally, I build images and then they are flashed to the box and then tested,
but I am working on a custom build using debian as a hypervisor for openwrt virtual machines.

First, I simply grabbed the latest live cd off of the Debian website, and then
copied it over to a flash drive. I then booted up my box from the live image.
I then used a few tips I found on the following websites, to install from a serial
console.

Basically, I added *vga=off console=ttyS0,115200n8* to our kernel boot parameters.
But we need to disable the *vga=off* in grub after we have finished our install.
Modern Grub doesn't seem to like it.

- [linux tips: debian install on serial console](https://linux-tips.com/t/debian-jessie-install-on-serial-console/270)
- [nixCraft: setup serial console on debian](https://www.cyberciti.biz/faq/howto-setup-serial-console-on-debian-linux/)

Second, I setup a few Qemu virtual machines using the *-curses* switch so that I can [run them on the terminal](http://stackoverflow.com/questions/22967925/running-qemu-remotely-via-ssh).
But that was too buggy for me so I used [*-nographic*](https://linux-tips.com/t/debian-jessie-install-on-serial-console/270) 
instead and to my surprise,the hotkeys are the same as GNU screen. It makes me wonder what is going on under the hood.

And finally, I started working on setting up a virtual network between my guests and the host systems.
There is yet more work to be done but I am heading in the right direction.

- [Qemu KVM bridged network with tap](http://blog.elastocloud.org/2015/07/qemukvm-bridged-network-with-tap.html)
- [Tap Networking with Qemu](https://wiki.archlinux.org/index.php/QEMU#Tap_networking_with_QEMU)

I also began reading "The Grapes of Math: How Life Reflects Numbers and Numbers Reflect Life" by Alex Bellos and
I am really enjoying it. It's nowhere near as technical as the other books that I recently aquired, but is still
very insightful and refreshing in it's own way. 
