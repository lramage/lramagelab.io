---
layout: post
title:  "Steam Chroot"
date:   2017-03-16 10:11:20
categories: openwrt progress mwan3 steam chroot 
---

I still need to get code highlighting setup in jekyll..
In the mean time,

TL;DR - Very little progress was made..

<pre>
# My week at work so far
while [ 1 ];
do
  Clock_In
  [RTFM](https://wiki.openwrt.org/doc/howto/mwan3)
  echo "Oh! I should try $X"
  Tweak_Mwan3 && make new_openwrt_image; sleep 4h
  Flash_Image

  [[ -x "$Working_Image" ]] && exit 0
done
</pre>

But at least personally I did manage to build a chroot for steam!
Here's the gist from Github. 
It's still pretty rough.

<script src="https://gist.github.com/lramage94/6da1a8deb8a1104e5c83765f090de30f.js"></script>
