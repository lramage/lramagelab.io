---
layout: post
title: Advanced Data Processing with SQLite
date: 2018-04-11 10:18:55
categories: libreoffice calc spreadsheet database sqlite python
last_modified_at: 2018-04-12 08:53:30
---

I have been working as a System Administrator for the last two months.
Part of my new responsibilities at this new company include inventory
management. I manage the onsite data room, a couple hundred wall-mounted 
video kiosks, over two hundred analog phones, and almost one thousand 
android tablets. I am constantly processing reports for these devices 
and which can be very time consuming.

Most of the software systems that I interact with at my new job are poorly written
proprietary applications that have not been updated in at least a decade,
if not longer. I am fortunate that I can export some of the data into a
spreadsheet for some of these applications. It takes a bit of work since
whoever created these applications decided it would be a good idea to
try to format the spreadsheets by adding empty columns and rows and adjusting the
cells. LibreOffice Calc allows me to export these spreadsheets to csv which
can then be imported into an sqlite database. This allows me to write python
scripts which can parse the data and automate repetitive tasks. I can also run 
simple SQL queries on data from different spreadsheets by importing them
as separate tables.

Find the user's location and his tablet's MAC address

```
sqlite> .open data.db
sqlite> .headers on
sqlite> .mode csv
sqlite> .import users.csv users
sqlite> .import devices.csv devices
sqlite> .output data.csv
sqlite> select * from users inner join devices on users.id = devices.user_id;
sqlite> .quit
```

References:

- [Libreoffice - Importing and Exporting CSV Files](https://help.libreoffice.org/Calc/Importing_and_Exporting_CSV_Files)

- [Python 3.6 Documentation - sqlite3](https://docs.python.org/3.6/library/sqlite3.html)

- [SQL As Understood By SQLite](https://www.sqlite.org/lang.html)

- [SQLite Tutorial - Export CSV](http://www.sqlitetutorial.net/sqlite-export-csv/)
