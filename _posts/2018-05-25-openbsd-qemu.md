---
layout: post
title: OpenBSD in Qemu
date: 2018-05-25 10:23:30
categories: linux qemu openbsd virtualization
---

Qemu Script

```
#!/bin/bash
# sudo ./qemu-openbsd.sh

if [[ "$(whoami)" != "root" ]]; then
  echo "Please run with sudo"
  exit 1
fi

ip tuntap add dev tap1 mode tap group kvm
ip link set dev tap1 up promisc on
ip addr add 0.0.0.0 dev tap1
ip link set tap1 master br0

qemu-system-x86_64 \
        -drive format=raw,file=disk.img \
        -enable-kvm \
        -m 1G \
        -net nic,vlan=0 -net tap,ifname=tap1,script=no,downscript=no

ip link del tap1

```

User Setup

    pkg_add bash git
    
    useradd -s /usr/local/bin/bash -mG wheel lramage && \
    passwd lramage
    
    su - lramage && \
    git clone https://github.com/lramage94/dotfiles
    

References:

- [Bryan Vyhmeister: Notes on OpenBSD in QEMU on OpenBSD](http://brycv.com/blog/2013/notes-on-openbsd-in-qemu-on-openbsd/)

- [Gist: Scripts to setup OpenBSD flashrd images and start qemu instances for each of them](https://gist.github.com/afresh1/6059415)

- [OpenBSD FAQ: Installation Guide](https://www.openbsd.org/faq/faq4.html)

- [OpenBSD FAQ: Package Management](https://www.openbsd.org/faq/faq15.html)

- [OpenBSD: Mirrors](https://www.openbsd.org/ftp.html)
