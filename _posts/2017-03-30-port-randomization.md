---
layout: post
title: "Port Randomization"
date: 2017-03-30 10:14:17
categories: port randomization
---

I stumbled onto this resource while I was doing
some research this morning. Port randomization
is an interesting concept.

- [hotplug](https://linux.die.net/man/8/hotplug)
- [Recommendations for Transport-Protocol Port Randomization](https://tools.ietf.org/html/rfc6056)
