---
layout: post
title: Python CGI Scripting With Apache and Gentoo
date: '2016-11-29T12:37:51-05:00'
---
This is really simple to get setup. 

First we’ll need to install apache with CGI enabled. Let’s edit /etc/portage/package.use/apache with the cgi use flag. 

touch /etc/portage/package.use/apache

vi /etc/apache/package.use/apache

www-server/apache cgi

Then save and close the file.

Now we will emerge Python and Apache

sudo emerge python apache

After that finishes we’ll need to go into /etc/apache/vhosts.d/ and edit default_vhost.include

Find the part with /var/www/localhost/cgi-bin. This is where we will put the config options to run Python scripts

Options +ExecCGI
AddHandler cgi-script .py

Then reload apache and you’re all set!
