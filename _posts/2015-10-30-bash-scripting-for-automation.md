---
layout: post
title: Bash Scripting for Automation
date: '2015-10-30T09:16:02-04:00'
---
Today I wrote a simple bash script for automating package installs. It uses an array and a simple for loop to download each package with apt-get. I like to work off of a live cd of linux when I am at the office and I am using someone else’s work station. That way I can have a consistent environment no matter which computer I use, not have to disturb anyone’s personal data, and also, when I log off I won’t have any of my data saved on their machine. It keeps things clean. The only downside is, every time I boot up from my thumb drive I have to reinstall different programs that I use. Well one simple way to help automate this process is to use a bash script to automate the installation of those packages.
