---
layout: post
title: Scientific Method and Software Development
date: 2017-03-22 11:51:50
categories: log computer science progress scientific method
---

Today is a another day of living in this incredible world that we call home. The more I study software, the more I start to see
abstract patterns and methods that don't just apply to software but to the rest of life. I believe I have started on a path of
unraveling the mysteries of our world. People who do this for a living are called "Scientist" so by definition I believe that I
am slowing becoming more of just a developer and a programmer and more of an engineer and a computer scientist. Some might say the
difference is just semantics but I think it's more than that. You can write code all day and still have no interest in what's going
on underneath the surface. Higher level languages are great because they enable us to rapidly prototype ideas and build really cool
things in a short amount of time. But the pursuit of a deeper understanding may take a life time. Here is an example of my typical
day at work, building software.

1) New bug reported - this usually is assigned to me but I have found bugs myself quite frequently.

2) Reproduce bug - Read bug report and try to reproduce issue.

3) Troubleshoot - This may take a long time. In order to fix something broken, I have to understand how the code is supposed
to work first. Then I can gain an understanding as to why it is not doing what is expected.

4) Find solution - bug fixes are often times, just trial and error until we stumble upon the solution. I wish it was more than that
but it's mostly just attempts at educated guesses.

5) Test solution - see that my fix works. For embedded devices, I can tinker with files on the box and fix it temporarily, but then
I will build a new image with my changes so that I can test it properly from start to finish.

6) Refine - Did it work? Why? Why not? How can this be done better? If the previous step fails, go back to #3.

And that's basically it. Now let's match this up with the scientific method.

1) Formulation of a question - "What's wrong with this code?"

2) Hypothesis - "I think X is affecting Y."

3) Prediction - "If I do Z to X, it will fix Y"

4) Testing - "Test X."

5) Analysis - "Y is still not fixed so it must not be X. (See #1)"

Here is an article about the scientific method and how it related to computer science from Princeton.

["The Role of the Scientific Method in Software Development"](https://www.cs.princeton.edu/~rs/talks/PathsPlus07.pdf) - Robert Sedgewick

