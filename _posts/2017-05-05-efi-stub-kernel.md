---
layout: post
title: "EFI Stub Kernel"
date: 2017-05-05 01:57:50
categories: linux uefi kernel stub apple macbook pro
---

One of the greatest ironies of our time is the current trend of using 
an Apple Macbook Pro laptop, to develop free and open source software.

I would never buy an apple product, however, I will take what I can get,
as long as I can install Linux on it. At my current job, I have been 
leased a macbook pro (early 2012) for work use. I am allowed and even
encouraged to take it home. I have been at the company for around a year
and during the first few months I left Mac OS on there and just did my
work and then turned it off. I do not understand how anyone can get
anything done on that operating system. Well I had enough of that and
installed Arch Linux. But I do not like systemd so after several months I
just installed Gentoo.

- [Apple Macbook Pro (Early 2013)](https://wiki.gentoo.org/wiki/Apple_Macbook_Pro_Retina_(early_2013))
- [EFI Stub Kernel](https://wiki.gentoo.org/wiki/EFI_stub_kernel)
