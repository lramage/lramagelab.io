---
layout: post
title: Artificial Intelligence
date: 2017-04-25 02:57:57
categories: artificial intelligence lisp programming logic
---

So as I have been reading books on mathematics and learning functional programming with Lisp, I have been trying to find software written with Lisp so
that I can practice with real code. I found myself revisiting Festival, a speech synthesis application written by the University of Edinburgh. Up until
now, I have been reading and interacting with Common Lisp implementations such as SBCL and ECL. But Festival contains a Scheme command interpreter which I
find fascinating. All of this time I have been discovering so much about Lisp, and then I open a door to even more Lisp but different!

I decided to start applying my accumulating knowledge to build a simple AI. I doubt it will be anything fancy. I would just like to launch programs
and dictate to my computer or phone. Now I already have a means of receiving feedback from my computer via Festival, but what about user input?
For that I searched for a Speech-To-Text application. CMU Sphinx is a project by Carnegie Mellon University which is defined on their website as
an open source speech recognition toolkit. It even has an android example project which can be downloaded on [F-Droid](https://f-droid.org/repository/browse/?fdfilter=sphinx&fdid=edu.cmu.pocketsphinx.demo).

I also have been reading about logic and machine learning. Simply launching a program or telling me the weather does not seem very intelligent to me.
I have begun studying First Order Logic. A good introduction can be found at [mathworld.wolfram.com](http://mathworld.wolfram.com/First-OrderLogic.html).

- [Festival](http://www.cstr.ed.ac.uk/projects/festival/)
- [CMU Sphinx](http://cmusphinx.sourceforge.net/)
- [First Order Logic](https://en.wikipedia.org/wiki/First-order_logic)
