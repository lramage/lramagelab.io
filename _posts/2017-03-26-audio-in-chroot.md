---
layout: post
title: Audio in Chroot
date: 2017-03-26 1:21:03
categories: log gentoo arch linux chroot gaming alsa
---

I had an audio issue today while gaming in my arch chroot. I cannot "share" my sound device between the gentoo host and the arch linux guest.
If I an application using my sound card in arch, I could not play audio from outside of the chroot. Also, I have a custom script in /usr/local/bin
that I use to invoke my chroot. Part of the script grabs the DISPLAY variable from our host and passes it to our chroot. But for some reason,
when this happens, the host loses track of what $DISPLAY is.

I started working on a simple rest api for managing my pc from my phone. It utilizes quite a few linux utilities to control my desktop.
I hope to work on it while I am in Atlanta during the middle of the week.
