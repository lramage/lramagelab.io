---
layout: post
title: Manage LXC Containers with Portage
date: 2018-03-29 10:00:31
last_modified_at: 2018-05-09 10:45:36
categories: lxc containers linux gentoo ebuild portage
---

*NOTE: This is a work in progress and is not officially supported or sanctioned by Gentoo Linux or the Portage developers.*

To add the custom overlay,

```
    $ git clone https://github.com/lramage94/dotfiles ~/.dotfiles

    $ cat << EOF > /etc/portage/repos.conf/dotfiles.conf
    > [dotfiles]
    > location = /usr/local/portage/dotfiles
    > sync-type = rsync
    > sync-uri = file:///home/lramage/.dotfiles/.local/usr/local/portage/dotfiles
    > priority = 9999
    > EOF
```

Then run `emerge --sync dotfiles` to sync the overlay.

To create the container,

```
    # First it needs to be unmasked.
    $ echo "lxc-container/paho-mqtt ~amd64" > /etc/portage/package.keywords/paho-mqtt
    $ emerge lxc-container/paho-mqtt
```

References:

- [dotfiles/.local/usr/local/portage/dotfiles/lxc-containers/paho-mqtt_testing](https://github.com/lramage94/dotfiles/commit/33b26251c39f2ad72289ac6003afeafe2ab9df6c)
- [Gentoo Wiki: LXC](https://wiki.gentoo.org/index.php?title=Lxc&redirect=no)
