---
layout: post
title:  "Kernel Routing and Steam Chroot"
date:   2017-03-15 10:11:20
categories: log 
---

Well my plight continues. Today I came to the realization of my errors in deleting the default route.
Once we have reconnected after failing over, there is nothing in place to re add our default route!
So I decided to simply reorder our default route metrics. Our wan route has a metric of 100 and our wan2
route has a metric of 200 so on failover I just replaced the wan default route metric with 300.

<pre>
$ ip route replace default via 10.10.10.100/24 dev eth0 src 10.10.10.1 metric 300
</pre>

Here are a few resources I found today that helped me:
- [Advanced Routing](http://www.rjsystems.nl/en/2100-adv-routing.php)
- [Tools: IP Route](http://linux-ip.net/html/tools-ip-route.html)
- [iproute2](http://baturin.org/docs/iproute2/)


I am still working on getting my desktop setup to my liking and I am learning many things along the way.
I am having a very hard time getting alsa setup. Here is some of my debugging information

<pre>
$ lspci -k
00:14.2 Audio device: Advanced Micro Devices, Inc. [AMD] FCH Azalia Controller (rev 01)
        Subsystem: Hewlett-Packard Company FCH Azalia Controller
        Kernel driver in use: snd_hda_intel

01:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Caicos HDMI Audio [Radeon HD 6400 Series]
        Subsystem: ASUSTeK Computer Inc. Caicos HDMI Audio [Radeon HD 6400 Series]
        Kernel driver in use: snd_hda_intel

$ aplay -l
**** List of PLAYBACK Hardware Devices ****
card 1: Generic [HD-Audio Generic], device 0: Generic Analog [Generic Analog]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: Generic [HD-Audio Generic], device 1: Generic Digital [Generic Digital]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 2: HDMI [HDA ATI HDMI], device 3: HDMI 0 [HDMI 0]
  Subdevices: 1/1
  Subdevice #0: subdevice #0

</pre>

Any help would be grealy appreciated.


And finally, I installed arch linux in a chroot so that I can have the benefits of a binary based distribution
on my gentoo setup. My purpose for this is primarily gaming with playonlinux and steam. I would however like to
migrate to using lxc as I believe containers would be a better solution. I looked at installing steam on gentoo
and I don't think I would like to do that as it requires udev and I am using [eudev](https://wiki.gentoo.org/wiki/Eudev).
Please correct me if I am wrong, but basically, it is the systemd free replacement.

Here are a few resources I found for setting up steam in a container:
- [Running Steam In A LXC Container](https://stgraber.org/2012/11/16/running-steam-in-a-lxc-container/)
- [TTY Sessions](https://mostlylinux.wordpress.com/troubleshooting/ttysessions/)
