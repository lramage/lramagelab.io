---
layout: post
title: "Mathematics for Programmers"
date: 2017-04-03 08-41-32
categories: mathematics programming lisp flavored erlang
---

Tonight I have begun learning [List Flavored Erlang](http://lfe.io/). I have some
work opportunities that require erlang, and for my own personal pleasure, I decided
to find a lisp built with erlang. It is a rather intriguing project. And regular erlang
libraries are compatible so I can play and be practical all at once! For my particular
purposes I am trying to implement a distributed dns system for my home network. The erlang
virtual machine is a marvelous creation that has roots in the telecommunications industry
and therefore I believe it to be a useful tool for myself, being in that particular field.

I have also recently taken it upon myself to study mathematics and philosphy.
I have been programming in some form or another since high school, and while I am now
what would be described as a "professional", i.e., for a living, I am but a neophyte in
computer science. I hope to gain a richer understanding of lower level concepts but I feel
that I can only accomplish this by first pursuing higher math.

I encourage my fellow programmers to read the following article on 
["The Mathematical Hacker"](http://www.evanmiller.org/mathematical-hacker.html)
written by Evan Miller on his website.

I have initialized a new repository for the purpose of tracking some of my precalculus
homework expressed in lisp.

[Precalculus Homework](https://github.com/lramage94/precalculus)
