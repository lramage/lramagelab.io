---
layout: post
title: "Virtual Networking"
date: 2017-04-06 11:34:27
categories: qemu virtual networking tcpdump
---

Today was yet another day studying and working with virtual networks in Qemu.
I too seek to [learn the way of the packet](https://danielmiessler.com/blog/professional-firewall-iptables/)
and upon my research I stumbled onto a great resource for learning tcpdump.

- [tcpdump](https://danielmiessler.com/study/tcpdump)

I also was reading up about the tun/tap interface.
The two devices work on different levels of the networking stack.
- TUN devices are layer 3, and are typically used for vpns
- TAP devices are layer 2, and are typically used for virtualized networking.

[Natural Born Coder: Understanding Tun Tap Interfaces](http://www.naturalborncoder.com/virtualization/2014/10/17/understanding-tun-tap-interfaces/)
