---
layout: post
title: Gentoo, ALSA, Gaming
date: 2017-03-18 11:59:23
categories: gentoo linux alsa gaming 
---

Today I made great progress! I finally got my soundcard working in gentoo! Well, kinda.. The headphone jack still isn't working but my speakers are so I'm pleased for now.
I am just amazed at how simple linux is. I've heard the saying "Everything is a file" but today I had a revelation. I was having a audio issues in my arch linux chroot,
and I just went into /proc/asound/ and changed the permissions on my audio card manually. Literally EVERYTHING IS A FILE! Also, I was able to play Star Wars: The Old Republic
again tonight for the first time in awhile. It seriously feels like christmas! I did come to the realization that I need a better graphics card. Many of the more modern games
that I own will not run without a proper video card. I just have a simple low power radeon card with 1gb of vram. I think next on my list is a 4gb card. I have been reading
about graphics cards with proper linux drivers and I have seen a lot of talk about nvidia. Their drivers are proprietary but from what I've read they seem to be very well made.

Two big issues that I am still ironing out (besides my headphone jack not working),

- [Borderlands 2](http://pcgamingwiki.com/wiki/Borderlands_2#Disable_controller_support) rendering issues.
- Star Wars: Knights of the Old Republic II does not like my multi-monitor setup.

Also, for swtor, I have it running in playonlinux and I renamed my user account in my chroot. This broke my playonlinux wine prefices. I created a symlink to the old username in
/home and that fixed that.
