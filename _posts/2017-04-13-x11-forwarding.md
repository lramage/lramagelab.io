---
layout: post
title: X11 Forwarding
date: 2017-04-13 11:40:21
categories: xorg ssh forwarding unix
---

Today I setup x11 forwarding on my freebsd macbook pro. I think I am in love again.
On the days that I work from home, I simply put my work laptop on my desk, turn it on,
and then ssh into it from my desktop pc. I have way more screen real estate and I can
use my keyboard and mouse with out any trouble. But if I need to go on a work specific
website, I had to use the laptop because I can't have work credentials on my personal machine.
Most people know about remote desktop, but I don't need the complete desktop, just one simple google chrome window.
So I left my laptop open, and using the *$DISPLAY* variable, could launch chrome from my desktop over ssh but
that's as close as I could get. That is, until this day.It literally took me less than five minutes to configure.

Oh Unix, how I love thee!

- [X11 Protocol](https://www.x.org/archive/X11R7.5/doc/x11proto/proto.pdf)
