---
layout: post
title:  "Tumblr Import?"
date:   2017-03-08 22:41:22
categories: log tumblr
---

I tried importing all of my old tumblr posts using the [Jekyll-Import plugin](http://import.jekyllrb.com/docs/tumblr/).

```
{% highlight ruby %}
$ ruby -rubygems -e 'require "jekyll-import";
JekyllImport::Importers::Tumblr.run({
  "url"            => "http://lramage94.tumblr.com",
  "format"         => "md", # or "html"
  "grab_images"    => true,  # whether to download images as well.
  "add_highlights" => true,  # whether to wrap code blocks (indented 4 spaces) in a Liquid "highlight" tag
  "rewrite_urls"   => true   # whether to write pages that redirect from the old Tumblr paths to the new Jekyll paths
})'
{% endhighlight %}
```

It didn't work too well. I had a few of my images but none of my links to external resources were imported.
I guess it's better than nothing.

After it imported I used perl to replace the layout from post to my own layout type,

```
{% highlight perl %}
$ perl -pe "s/tumblr/post/g" _posts/tumblr/*.md
{% endhighlight %}
```

Also as you can probably tell, syntax highlighting does not seem to be working?
