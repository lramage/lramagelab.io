---
layout: post
title: "Strife"
date: 2017-04-14 08:23:56
categories: programming code maintanence legacy
---

I had a very fulfilling day today at work. I was working in a codebase that everyone in the department
dreads. It is massive and hideous, with many faults around every corner. Legacy codebases, both fascinate
and terrify me. There is much to learn from history. Thus the saying by [George Santayana](https://en.wikiquote.org/wiki/George_Santayana),

	"Those who cannot learn from history are doomed to repeat it."

It is only natural that these monoliths from the past be flawed, for we ourselves are flawed.
As I study the life of Albert Einstein, and read about some of the famous engineers and programmers
of our time, I see patterns that occur in my personal life. Every one of us has potential for
great things, and we are all alike in some way. It is very encouraging to hear, but it also
serves as a warning. Everyone has some flaw, for that is human nature. But if we can see the mistakes
of others, and do our best to learn from them, then that is the best way to avoid folly.
For that is true wisdom.

Every situation you find yourself in, is an opportunity for growth. So even though my patience
and wit were taxed today, I am better for it. And I am sure that one day, some other young person
will look at my code in the future and will question my sanity just as I did today.
