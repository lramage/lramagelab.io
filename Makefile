# Need to split these into individual targets
# This should be draft
post :
	@read -p "Post title: " title; \
	awk -v title="$$title" \
	'/title/ { gsub(/\"title\"/, title) }; { print }' _drafts/template.md | \
	awk -v timestamp="$$(date +'%F %I:%M:%S')" \
	'/date/ { gsub(/YYYY-MM-DD[[:space:]]HH:MM:SS/, timestamp ) }; { print }' > _drafts/$$(date +'%F')-$$( echo $$title | awk '{ print tolower($$0) }' | awk '{ gsub(/ /,"_", $$0); print }' ).md
