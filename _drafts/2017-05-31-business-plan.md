---
layout: post
title: business-plan
date: 2017-05-31 08:36:04
categories: business plan creative commons resources
---

- [A List Apart: This Web Business](https://alistapart.com/article/business)
- [Google Docs: Business Plan Template](https://docs.google.com/document/d/1f_EW8KAbER4E1PLLnTjLYrO9SslMNY3WxWYSseR105U/template/preview)
- [IRS: Business or Hobby?](https://www.irs.gov/uac/business-or-hobby-answer-has-implications-for-deductions)
