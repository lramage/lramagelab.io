---
layout: post
title: "AOSP for Portage"
date: 2017-05-26 08:16:13
categories: aosp portage gentoo prefix amazon fire phone kodiak
---

- [AOSP Source](https://source.android.com/source/)
- [Gentoo Wiki: Android Build](https://wiki.gentoo.org/wiki/Project:Android/build)
- [Gentoo Packages: sys-kernel](https://packages.gentoo.org/categories/sys-kernel)
- [XDA: Fire phone custom kernel](https://forum.xda-developers.com/fire-phone/development/dev-building-custom-kernel-kernel-t3195492)
