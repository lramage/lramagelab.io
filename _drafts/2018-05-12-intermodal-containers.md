---
layout: post
title: Intermodal Containers
date: 2018-05-12 01:43:13
categories: linux kernel freebsd containers lxc jails
---

References:

- https://blog.jessfraz.com/post/building-container-images-securely-on-kubernetes/

- https://blog.jessfraz.com/post/containers-zones-jails-vms/

- https://en.m.wikipedia.org/wiki/Intermodal_container

- https://github.com/genuinetools/img

- https://github.com/stevvooe/distribution/tree/dist-demo
