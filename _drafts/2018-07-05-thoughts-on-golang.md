---
layout: post
title: Thoughts on Golang
date: 2018-07-05 15:08:11
categories: programming languages golang java android c gentoo linux
last_modified_at: 2018-07-12 12:41:33
---

I have been confused about the purpose of Go for a long time, but I finally figured it out!

Let me provide a bit of context. For kernel development, everything is done in C. And nearly all low-level user space applications (think busybox, iproute2, dnsmasq, etc.), are also written in C. But recent applications such as docker, lxd, snap, etc. are all written in Go.

Why the switch to a higher level language for these applications?

The other day, I had a conversation with a game developer friend from London on Mastodon who just started writing game networking middle-ware Go and he loves it.
I couldn't figure out what the big deal was until he told me that his background is in Java.

Bingo!

For enterprise-level computing, switching to Go from Java is a no-brainer—Anything is better than Java!

That explains why so many of these large projects are written in Go.

But I still had one more issue with this. What about Java on Android?

At my new job I deal with thousands of tablets everyday so I am constantly searching the AOSP documentation looking for tools to help automate debugging.

If you lookup the first-app guide in the documentation, you will see that all the code examples are in Kotlin or Java, but by default, Kotlin is displayed.

On Android, they couldn't simply throw Java away, but it is slowly being replaced by Kotlin, which runs on ART, the new Android JVM.

## Excerpts from shark-bait discussion

"There are no conditionals or control flow statements - any complexity is handled in build logic written in Go" [].

"The primary reason why Go packages cannot be reproducibly built is because the import statement(which drives go get)
does not contain sufficient information to identify which revision of a Go package it should fetch...The Go Overlay main purpose 
is to create an easy way for the Go community to use Gentoo" [].

"Versioning is a source of significant complexity, especially in large code bases, and we are unaware of any approach that works
well at scale in a large enough variety of situations to be appropriate to force on all Go users...If you're using an externally supplied
package and worry that it might change in unexpected ways, the simplest solution is to copy it to your local repository.
(This is the approach Google takes internally.)" [].

So based on the information above, it looks like forking is the recommended solution to our problem. 
This really doesn't make any sense.

References:

- https://golang.org
- https://github.com/docker/docker-ce
- https://developer.android.com/training/basics/firstapp/starting-activity
- https://kotlinlang.org

[]: https://android.googlesource.com/platform/build/soong/+/master/README.md
[]: https://github.com/gentoo-mirror/go-overlay
[]: https://golang.org/doc/faq#get_version