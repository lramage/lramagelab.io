---
layout: post
title: More Vim
date: 2017-06-26 09:58:01
categories: vim programming configuration files automation
---

- [Learn Vimscript the Hard Way: Autocommands](http://learnvimscriptthehardway.stevelosh.com/chapters/12.html)
- [Andrew Stewart: Project Specific Vim Configuration](https://andrew.stwrt.ca/posts/project-specific-vimrc/)
