---
layout: page
title: Bio
permalink: /bio/
---

Greetings,

My name is Lucas Ramage, and I am a software engineer with experience in UNIX-like operating systems and open source software in both enterprise and start-up environments.

<a href="https://pgp.mit.edu/pks/lookup?op=get&search={{ site.pgp.public_key }}">
PGP Fingerprint: {{ site.pgp.fingerprint }}
</a>
