---
layout: page
title: Additional Content
permalink: /external/
---

These are websites that I have built in the past and/or that I currently maintain.

Archive
- [Guitar For Life](https://sites.google.com/site/lramage94/home)

Misc
- [Crazy Tinker](https://crazy-tinker.gitlab.io)

Other
- [Calling the Hope Revolution Into Service](http://chrscharity.org)
- [Morton James Public Library](http://morton-jamespubliclibrary.com)
- [Spiral Communications](http://spiral-communications.com)


Video Gaming
- [HexGearInc](https://hexgearinc.gitlab.io)
- [Seven Sun Downs](https://hexgearinc.gitlab.io/seven-sun-downs)
