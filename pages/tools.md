---
layout: page
title: Tools
permalink: /tools/
---

Setup
- [Dotfiles](https://gitlab.com/lramage/dotfiles)
- [OpenPGP](http://openpgp.org)

Sites
- [ASCII](http://ascii.co.uk)
- [Clean CSS](http://www.cleancss.com)
- [JSON Parser](http://json.parser.online.fr)
- [New Post (Template)](https://gitlab.com/lramage/lramage.gitlab.io/raw/master/_drafts/template.md)
- [Resume Score](http://rezscore.com)
- [Source Code (gitlab)](https://lramage.gitlab.io/source)
- [to-markdown](https://domchristie.github.io/to-markdown)
- [Video Conferencing](https://meet.jit.si)
- [YAML Parser](http://yaml-online-parser.appspot.com)
