---
layout: page
title: Bookmarks
permalink: /bookmarks/
---

*These bookmarks have been automatically exported and converted to markdown.*

### Bookmarks Bar

[](http://www.mudconnect.com/)

[](http://www.sindome.org/)

[](http://www.zeldadungeon.net/)

[](https://trello.com/)

[](https://mail.google.com/mail/u/0/#inbox)

[](http://www.youtube.com/)

[](https://hangouts.google.com/)

[](http://superchillin.com/login.php)

[](http://www.crunchyroll.com/)

[](https://drive.google.com/)

[](https://play.google.com/store?hl=en&tab=w8)

[](https://github.com/)

[](https://bitbucket.org/dashboard/overview)

[](http://pastebin.com/)

[](http://developer.android.com/index.html)

[](http://blenderartists.org/forum/index.php)

[](http://www.regions.com/)

[](https://www.bankofamerica.com/)

[](http://www.arborbanking.com/)

[](http://www.amazon.com/)

[](http://www.w3schools.com/)

[](http://www.deviantart.com/)

[](https://www.reddit.com/)

[](https://news.ycombinator.com/)

[](http://lramage94.github.io/)

[](http://tiddlywiki.com/)

### Hardware

[$200 Gaming PC Build 2017](http://www.toptengamer.com/best-200-gaming-build/)

[$150 Gaming Computer Build 2017](http://www.toptengamer.com/150-gaming-pc/)

[ChooseMyPC](https://choosemypc.net/#top)

[PCPartPicker](https://pcpartpicker.com/)

[Studio Kousagi Wiki](https://kosagi.com/w/index.php?title=Main_Page)

[Thermaltake - U.S.A. - Core G3 - CA-1G6-00T1WN-00](http://www.thermaltakeusa.com/Chassis/Slim_ATX_/Core/C_00002936/Core_G3/design.htm)

[Newegg.Com - Thermaltake Slim X3 CLP0534 80mm CPU Cooler](https://m.newegg.com/Product/index?itemnumber=N82E16835106152)

[Dell Server BIOS PowerEdge PE2900 Version 2.7.0 Driver Details](http://www.dell.com/support/home/us/en/555/Drivers/DriversDetails?driverId=5VWCM)

[IPMI Dell Power Edge 2900](http://www.dell.com/downloads/global/power/ps4q04-20040204-murphy.pdf)

[PC case mod. DIY Archives - bit-tech.net](http://forums.bit-tech.net/showthread.php?t=226570)

[Mnpctech Case Mod Gallery](https://mnpctech.com/mnpctech-case-mod-gallery.html)

[Custom Case Building 101](http://www.overclockers.com/forums/showthread.php/173336-Custom-Case-Building-101-basics)

[New L 101 Multi Function Computer Case](https://www.aliexpress.com/item/New-L-101-Multi-Function-263-470-130mm-Centralized-Computer-Cases-Towers-Biggest-Support-For-Main/32757213202.html?spm=2114.01010108.3.70.71YtUa&ws_ab_test=searchweb0_0,searchweb201602_2_10065_10068_10130_433_10136_10060_10062_10056_10055_10054_302_10059_10099_10103_10102_10096_10052_10053_10107_10050_10106_10051_10084_10083_10080_10082_10081_10110_10111_10112_10113_10114_10078_10079_10073_10070_10122_10123_10126_10124,searchweb201603_1,afswitch_1,ppcSwitch_5,single_sort_0_price_asc&btsid=108c2cc9-19c2-4c95-88f8-4b4cd4523b64&algo_expid=513ad5e8-891a-4d78-8a8e-429807a5f1c0-8&algo_pvid=513ad5e8-891a-4d78-8a8e-429807a5f1c0)

[HP ENVY 700-056 Desktop PC Product Specifications](http://h20564.www2.hp.com/hpsc/doc/public/display?docId=c03782932)

[Gentoo Forums :: View topic - joystick (xpad) not recognized by zsnes, mupen64, [SOLVED]](https://forums.gentoo.org/viewtopic-t-416099-start-0.html)

[Gamepad - ArchWiki](https://wiki.archlinux.org/index.php/Gamepad#Xbox_360_controller)

### Android

[[GUIDE] Making Dump Files Out of Android Device Partitions](http://forum.xda-developers.com/showthread.php?t=2450045)

[HOWTO: Unpack, Edit, and Re-Pack Boot Images - Android Wiki](http://android-dls.com/wiki/index.php?title=HOWTO:_Unpack%2C_Edit%2C_and_Re-Pack_Boot_Images)

[How to run Debian or Ubuntu GNU/Linux on your Android](http://whiteboard.ping.se/Android/Debian)

[Booting native Arch Linux on an Android device - Unix & Linux Stack Exchange](http://unix.stackexchange.com/questions/64546/booting-native-arch-linux-on-an-android-device)

[Plasma/Mobile/CyanogenModBase - KDE Community Wiki](https://community.kde.org/Plasma/Mobile/CyanogenModBase#Manual_.28Useful_for_enabling_device_and_trying_out_porting.29)

[Project:Android - Gentoo Wiki](https://wiki.gentoo.org/wiki/Project:Android)

[Building a custom kernel for Amazon Fire Phone](https://forum.xda-developers.com/fire-phone/development/dev-building-custom-kernel-kernel-t3195492)

[Linux Kernel Driver DataBase: CONFIG_USB_OTG: OTG support](http://cateee.net/lkddb/web-lkddb/USB_OTG.html)


### Coding

### Game

[AKIMBO AssaultCube](http://ac-akimbo.net/)

[Python RPG Game Tutorial](http://usingpython.com/python-rpg-game/)

[Lazy Foo' Productions - Beginning Game Programming v2.0](http://lazyfoo.net/tutorials/SDL/index.php)

[Quadropolis | Cube engine mapping, modding, and mayhem](http://quadropolis.us/)

[Cube 2: Sauerbraten - Editing Reference](http://sauerbraten.org/docs/editref.html)

[Radio Fallout.FM - Radio Fallout 4 Classical and Diamond City Radio | The songs of Fallout | Fallout Original Soundtrack Music](http://fallout.fm/)

[ACCAD - Motion Capture Lab - data and downloads](http://accad.osu.edu/research/mocap/mocap_data.htm)

[Sci-Fi Portraits Collection | OpenGameArt.org](http://opengameart.org/content/sci-fi-portraits-collection)

[Compiling Lakka · libretro/Lakka Wiki](https://github.com/libretro/Lakka/wiki/Compiling-Lakka)

[Priiloader/hacks - WiiBrew](http://wiibrew.org/wiki/Preloader/Priiloader/hacks)

[Importing a Modeled Mesh From Blender to Three.js - Jonathan Petitcolas](https://www.jonathan-petitcolas.com/2015/07/27/importing-blender-modelized-mesh-in-threejs.html)

[Worldforge :: Home](https://www.worldforge.org/)





[Best coding practices - Wikipedia, the free encyclopedia](http://en.m.wikipedia.org/wiki/Best_coding_practices)

[How to Develop an IRC Bot - wikiHow](http://www.wikihow.com/Develop-an-IRC-Bot)

[Getting started with WebGL - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL)

[DevDocs - C++ / Namespaces](http://devdocs.io/cpp)

[Designing a RESTful API with Python and Flask - miguelgrinberg.com](http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask)

[python-for-android — python-for-android 0.1 documentation](http://python-for-android.readthedocs.io/en/latest/)

[5\. Data Structures — Python 3.6.0b1 documentation](https://docs.python.org/3.6/tutorial/datastructures.html)

[simple-crypt 4.1.7 : Python Package Index](https://pypi.python.org/pypi/simple-crypt)

[19.1.14\. email: Examples — Python 3.5.2 documentation](https://docs.python.org/3/library/email-examples.html)

[Passlib 1.6.5 documentation — Passlib v1.6.5 Documentation](https://pythonhosted.org/passlib/)

[NGINX settings - GitLab Documentation](https://docs.gitlab.com/omnibus/settings/nginx.html#using-a-non-bundled-web-server)

[Your First Machine Learning Project in Python Step-By-Step - Machine Learning Mastery](http://machinelearningmastery.com/machine-learning-in-python-step-by-step/)

[Best Practices for Designing a Pragmatic RESTful API | Vinay Sahni](http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api)

[scikit-learn: machine learning in Python — scikit-learn 0.18.1 documentation](http://scikit-learn.org/stable/)

[Minimal Structure — Python Packaging Tutorial](https://python-packaging.readthedocs.io/en/latest/minimal.html)

[glut_examples/examples/cube.c](https://www.opengl.org/archives/resources/code/samples/glut_examples/examples/cube.c)

[Python Scripting for the Game Engine : CG Masters](http://www.cgmasters.net/free-tutorials/python-scripting/)

[Streaming Microphone Input With Flask](https://henri.io/posts/streaming-microphone-input-with-flask.html)

[LISP](http://www.math-cs.gordon.edu/courses/cs323/LISP/lisp.html)

[LISP Tutorial 1: Basic LISP Programming](https://www.cs.sfu.ca/CourseCentral/310/pwfong/Lisp/1/tutorial1.html)

[They Called It LISP for a Reason: List Processing](http://www.gigamonkeys.com/book/they-called-it-lisp-for-a-reason-list-processing.html)

[Simple Plug-in Architecture in Plain C](https://www.codeproject.com/articles/389667/simple-plug-in-architecture-in-plain-c)

### Fonts

[BitDust Two](https://www.urbanfonts.com/fonts/BitDust_Two.htm)

[Square Future](https://www.urbanfonts.com/fonts/SQUAREFUTURE.htm)

[Square Pusher](http://www.dafont.com/squarepusher.font?text=Hex+OS)

[ASCII ART](http://ascii.co.uk/)

### Linux

### Gentoo

### packages



[www-apps/cgit](https://packages.gentoo.org/packages/www-apps/cgit)

[app-misc/cmatrix](https://packages.gentoo.org/packages/app-misc/cmatrix)

[sys-apps/lm_sensors](https://wiki.gentoo.org/wiki/Lm_sensors)

[sys-block/megacli](https://packages.gentoo.org/packages/sys-block/megacli)

[sys-apps/flashrom](https://packages.gentoo.org/packages/sys-apps/flashrom)

[sys-apps/dmidecode](https://packages.gentoo.org/packages/sys-apps/dmidecode)

[x11-misc/xystray](https://packages.gentoo.org/packages/x11-misc/xystray)





[Become a Developer](https://www.gentoo.org/get-involved/become-developer/)

[www.gentoo.org](https://www.gentoo.org/)

[Xorg Transparency](http://gentoo-en.vfose.ru/wiki/X.Org/Transparency#transset-df)

[Portage Source](https://gitweb.gentoo.org/proj/portage.git/tree/DEVELOPING?id=refs/heads/prefix)

[Git Server](https://wiki.gentoo.org/wiki/Git#Server)

[IP Tables](https://wiki.gentoo.org/wiki/Iptables)

[ALSA](https://wiki.gentoo.org/wiki/ALSA#Installation)

[LXC](https://wiki.gentoo.org/wiki/LXC)

[Kernel/Removal](https://wiki.gentoo.org/wiki/Kernel/Removal)

[ZFS](https://wiki.gentoo.org/wiki/ZFS)

[Gentoo Linux LiveCD for Dummies!](https://forums.gentoo.org/viewtopic-t-244837.html)

[Quick Installation Checklist](https://wiki.gentoo.org/wiki/Quick_Installation_Checklist#BIOS.2FMBR_2)

[Wiki](https://wiki.gentoo.org/wiki/Main_Page)

[Embedded Handbook](https://wiki.gentoo.org/wiki/Embedded_Handbook)

[Display manager](https://wiki.gentoo.org/wiki/Display_manager)

[qingy](https://wiki.gentoo.org/wiki/Qingy)

[packages\base\profiles](https://gitweb.gentoo.org/repo/gentoo.git/tree/profiles/base/packages)

[Stage tarball](https://wiki.gentoo.org/wiki/Stage_tarball#Stage_1)

[Flying with gentoo](https://forums.gentoo.org/viewtopic.php?t=231170)

[Cheat Sheet](https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet)

[Home Router](https://wiki.gentoo.org/wiki/Home_Router)

[Network bridge](https://wiki.gentoo.org/wiki/Network_bridge)

[Prefix/Bootstrap](https://wiki.gentoo.org/wiki/Project:Prefix/Bootstrap)

[Cluster](https://wiki.gentoo.org/wiki/Cluster)

[Xorg/Hardware 3D acceleration guide](https://wiki.gentoo.org/wiki/Xorg/Hardware_3D_acceleration_guide)

[QEMU](https://wiki.gentoo.org/wiki/QEMU)

[Bluetooth](https://wiki.gentoo.org/wiki/Bluetooth)

[Catalyst](https://wiki.gentoo.org/wiki/Catalyst)

[Binary package guide](https://wiki.gentoo.org/wiki/Binary_package_guide)

[mdev - Gentoo Wiki](https://wiki.gentoo.org/wiki/Mdev)





[Chromium OS Developer Guide - The Chromium Projects](http://www.chromium.org/chromium-os/developer-guide)

[Linux Kernel Driver DataBase: CONFIG_USB_OTG: OTG support](http://cateee.net/lkddb/web-lkddb/USB_OTG.html)

[Linux Boot Logo - ArmadeusWiki](http://www.armadeus.org/wiki/index.php?title=Linux_Boot_Logo)

[Writing device drivers in Linux: A brief tutorial](http://freesoftwaremagazine.com/articles/drivers_linux/?)

[Creating and using squashed file systems](http://tldp.org/HOWTO/SquashFS-HOWTO/creatingandusing.html)

[Reload partition table without rebooting](http://oldblog.renanmarks.net/en/blog/reload-partition-table-without-rebooting-linux-system)

[Steam/Troubleshooting - ArchWiki](https://wiki.archlinux.org/index.php/Steam/Troubleshooting)

[DellBIOS - Ubuntu Wiki](https://wiki.ubuntu.com/DellBIOS)

[PKGBUILD - ArchWiki](https://wiki.archlinux.org/index.php/PKGBUILD)

[QLOCKTWO in conky](http://crunchbang.org/forums/viewtopic.php?id=4201))

[Wii-Linux - WiiBrew](http://wiibrew.org/wiki/Wii-Linux)

[PKGBUILD.proto](https://git.archlinux.org/pacman.git/plain/proto/PKGBUILD.proto)

[kernel interfaces](https://en.wikipedia.org/wiki/Linux_kernel_interfaces#Linux_API)

[Ubuntu Touch](http://cdimage.ubuntu.com/ubuntu-touch/xenial/daily-preinstalled/current/)

[ugh.pdf #127](https://web.archive.org/web/20120120031001/http://m.simson.net/ugh.pdf)

[XWayland](https://wayland.freedesktop.org/xserver.html)

[hdparm](https://wiki.archlinux.org/index.php/hdparm)

[xxd](http://linuxcommand.org/man_pages/xxd1.html)







### Roms



[Syphon Filter - Dark Mirror (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Syphon_Filter_-_Dark_Mirror_(USA)/156243)

[SOCOM - U.S. Navy SEALs - Fireteam Bravo (USA) ISO Download < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/SOCOM_-_U.S._Navy_SEALs_-_Fireteam_Bravo_(USA)/156174-download)

[Star Wars - Battlefront II (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Star_Wars_-_Battlefront_II_(USA)/156230)

[Star Wars - Lethal Alliance (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Star_Wars_-_Lethal_Alliance_(USA)/156618)

[Star Wars - The Force Unleashed (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Star_Wars_-_The_Force_Unleashed_(USA)/158075)

[Armored Core - Formula Front Extreme Battle (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Armored_Core_-_Formula_Front_Extreme_Battle_(USA)/155701)

[Army of Two - The 40th Day (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Army_of_Two_-_The_40th_Day_(USA)/158182)

[Aliens vs. Predator - Requiem (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Aliens_vs._Predator_-_Requiem_(USA)/157179)

[Armored Core 3 Portable (USA) (PSN) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Armored_Core_3_Portable_(USA)_(PSN)/177501)

[Armored Core - Silent Line Portable (USA) (PSN) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Armored_Core_-_Silent_Line_Portable_(USA)_(PSN)/177503)

[Armored Core - Last Raven Portable (USA) (PSN) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Armored_Core_-_Last_Raven_Portable_(USA)_(PSN)/177502)

[Resistance - Retribution (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Resistance_-_Retribution_(USA)/158015)

[Black Rock Shooter - The Game (Japan) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Black_Rock_Shooter_-_The_Game_(Japan)/158673)

[Coded Arms - Contagion (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Coded_Arms_-_Contagion_(USA)/158700)

[Coded Arms (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Coded_Arms_(USA)/155766)

[Ghost in the Shell - Stand Alone Complex (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Ghost_in_the_Shell_-_Stand_Alone_Complex_(USA)/155882)

[G.I. Joe - The Rise of Cobra (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/G.I._Joe_-_The_Rise_of_Cobra_(USA)/157800)

[Star Wars Battlefront - Elite Squadron (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Star_Wars_Battlefront_-_Elite_Squadron_(USA)/158071)

[Star Wars The Clone Wars - Republic Heroes (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Star_Wars_The_Clone_Wars_-_Republic_Heroes_(USA)/158073)

[Sword Art Online - Infinity Moment (Japan) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Sword_Art_Online_-_Infinity_Moment_(Japan)/177492)

[Syphon Filter - Logan's Shadow (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Syphon_Filter_-_Logan's_Shadow_(USA)/157564)

[Star Wars Battlefront - Renegade Squadron (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Star_Wars_Battlefront_-_Renegade_Squadron_(USA)/157549)

[Tom Clancy's EndWar (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Tom_Clancy's_EndWar_(USA)/158105)

[Tom Clancy's Rainbow Six - Vegas (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Tom_Clancy's_Rainbow_Six_-_Vegas_(USA)/157481)

[Tom Clancy's Ghost Recon - Advanced Warfighter 2 (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Tom_Clancy's_Ghost_Recon_-_Advanced_Warfighter_2_(USA)/157305)

[Tom Clancy's Splinter Cell - Essentials (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Tom_Clancy's_Splinter_Cell_-_Essentials_(USA)/156223)

[Tom Clancys Ghost Recon - Predator (USA) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Tom_Clancys_Ghost_Recon_-_Predator_(USA)/158602)

[Tomoyo After - It's a Wonderful Life - CS Edition (Japan) ISO < PSP ISOs | Emuparadise](http://www.emuparadise.me/Sony_Playstation_Portable_ISOs/Tomoyo_After_-_It's_a_Wonderful_Life_-_CS_Edition_(Japan)/158106)

[Virtual On Cyber Troopers (US, Revision B) ROM < MAME ROMs | Emuparadise](http://www.emuparadise.me/M.A.M.E._-_Multiple_Arcade_Machine_Emulator_ROMs/Virtual_On_Cyber_Troopers_(US,_Revision_B)/17955)

[saov2.iso.zip - Google Drive](https://docs.google.com/file/d/0B7Wsqsv90T5bYWNpTG9abzNpV28/edit)





[devRant // new Rant("fml");](https://www.devrant.io/feed/)

[The Hacker Manifesto](http://www.mithral.com/~beberg/manifesto.html)

[Morpheon Dark](https://chrome.google.com/webstore/detail/dpnbbonpgadmkipdlclghcekaklebdpi?utm_source=chrome-ntp-icon)

[Mewbies](http://mewbies.com/)

[bash.org](http://www.bash.org/)

[Email Self-Defense](https://emailselfdefense.fsf.org/en/)

[Document archive](http://doc.cat-v.org/)

[Webpage archive](http://archive.is/)

[Freegal Music : Your New Music Library : Home](https://scgov.freegalmusic.com/homes/index)



### Design



[Naldz Graphics - All Designs,Graphics and Web Resources](http://naldzgraphics.net/)

[sushixav](http://sushixav.blogspot.com/?m=1)

[Workflow: MakeHuman and Blender - Epic Wiki](https://wiki.unrealengine.com/Workflow:_MakeHuman_and_Blender)

[CSS](http://www.cleancss.com/)

[Magic Animations CSS3](http://www.minimamente.com/example/magic_animations/)

[Page Transitions with CSS3](http://tympanus.net/codrops/2012/01/30/page-transitions-with-css3/)

[GUILTY CROWN](http://guilty-crown.jp/)

[Skeleton: Responsive CSS Boilerplate](http://getskeleton.com/)

[Main Menu | Cryptaris](http://www.cryptarismission.com/#!/main-menu)

[How to Create a Self-Portrait in a Geometric Style](https://design.tutsplus.com/tutorials/how-to-create-a-self-portrait-in-a-geometric-style--vector-5992)

[A Practical Guide to SVGs on the web](https://svgontheweb.com/)







### Game Wiki's



[Seven Sun Downs](http://seven-sun-downs.wikia.com/wiki/Seven_Sun_Downs_Wiki)

[WARFRAME](http://warframe.wikia.com/wiki/WARFRAME_Wiki)

[Ryzom](http://en.wiki.ryzom.com/wiki/Main_Page)







### PaperBoy!



[Android Cheatsheet for Graphic Designers](http://petrnohejl.github.io/Android-Cheatsheet-For-Graphic-Designers/)

[Using the Google Maps API - Webmonkey](http://www.webmonkey.com/2010/02/make_maps_with_google/)





[Lebende Bilder: Tutorial: How to create a logo in Inkscape](http://lebende-bilder.blogspot.com/2014/03/tutorial-how-to-create-logo-in-inkscape.html?m=1)

[Create a Game Character with HTML5 and JavaScript - Part 1 { William Malone }](http://www.williammalone.com/articles/create-html5-canvas-javascript-game-character/1/)

[Doc:2.6/Manual/Render/Post Process/Edges - BlenderWiki](http://wiki.blender.org/index.php/Doc:2.6/Manual/Render/Post_Process/Edges)

[Autochords : Chord Progression Generator](https://autochords.com/)

[Deus Ex: Human Revolution Wiki Guide & Walkthrough - IGN](http://m.ign.com/wikis/deus-ex-human-revolution)

[Fretboard Workshop: Move Beyond Pentatonics with Six-Note Scales - Premier Guitar](http://m.premierguitar.com/Magazine/Issue/2013/Jan/Fretboard_Workshop_Move_Beyond_Pentatonics_with_Six_Note_Scales.aspx)

[Fallout Shelter Efficiency Data](https://docs.google.com/spreadsheets/d/1Nai09D_aM2syl3iPP5hkveDcOUwhsoUDR6s1lwC0e8c/htmlview?sle=true#)

[xkcd: Business Idea](https://xkcd.com/)

[List of development tools - WiiBrew](http://wiibrew.org/wiki/List_of_development_tools)

[How to Build a JRPG: A Primer for Game Developers](http://gamedevelopment.tutsplus.com/articles/how-to-build-a-jrpg-a-primer-for-game-developers--gamedev-6676)

[Game Ready 3D Models | OpenGameArt.org](http://opengameart.org/content/game-ready-3d-models)
