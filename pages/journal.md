---
layout: page
title: Log
permalink: /log/
---
Welcome to my online journal. Here are some of my recent posts.

<span class="disclaimer">Disclaimer: The thoughts expressed in these writings are entirely my own, and do not represent the opinions of current or former employers.</span>

<ul class="posts">
	{% for post in site.posts %}
	<li> 
		<a href="{{ post.url }}">{{ post.date  | date: '%B %d, %Y' }} - {{ post.title }}</a>
		{{ post.excerpt }}
	</li>
	{% endfor %}
</ul>

<style>

.disclaimer {
    font-size: smaller;
    color: #F00;
}

</style>
