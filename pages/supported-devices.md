---
layout: page
title: Supported Devices
permalink: /supported-devices/
---

<pre>
  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
</pre>

<!-- https://git.openwrt.org/?p=openwrt/openwrt.git;a=blob_plain;f=package/base-files/files/etc/banner -->

| Device | Manufacturer | Price |
| ------ | ------------ | ----- |
| [AC1750 Wireless Dual Band Gigabit Router](https://www.tp-link.com/us/products/details/cat-9_Archer-C7.html) | TP-Link | $89.99 (MSRP) |

Donations are greatly appreciated!

- [Paypal](https://www.paypal.me/lramage)
- [Liberapay](https://liberapay.com/lramage)

See also,

[Dynamic DNS](https://en.wikipedia.org/wiki/Dynamic_DNS) can be useful for accessing your home network remotely for security cameras, smarthub, etc.


A [VPN](https://en.wikipedia.org/wiki/Virtual_private_network) offers security and privacy when browsing the internet.

I use [Tinc](https://tinc-vpn.org) but I also recommend [OpenVPN](https://openvpn.net).

DNS is the system used by computers to translate human meaningful names to IP Addresses.

[OpenDNS](https://www.opendns.com/home-internet-security/) provides a free tier for personal use.

<style>
pre {
    width: 450px;
    background: #010101;
    color: white;
}

table {
    max-width: 750px;
}
</style>