---
layout: page
title: Resources
permalink: /resources/
---
Books
- [C Programming in Linux](http://teacher.en.rmutt.ac.th/samatachai.j/ES/55/c.pdf)
- [Digital Deli: The Comprehensive, User-Lovable Menu of Computer Lore, Culture, Lifestyles and Fancy](http://www.atariarchives.org/deli)
- [Flatland: A romance of many dimensions](http://www.geom.uiuc.edu/~banchoff/Flatland)
- [Small is Beautiful](http://www.colinalexander.info/files/pdfs/Schumacher.pdf)
- [Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/sicp)
- [The UNIX-HATERS Handbook](http://web.mit.edu/~simsong/www/ugh.pdf)

Online Journals
- [Brit Butler](http://blog.redlinernotes.com)
- [Chris Krycho](http://www.chriskrycho.com)
- [Diego Elio Pettenò](https://blog.flameeyes.eu)
- [Digital Survival](http://www.digitalsurvival.io)
- [Heredoc](https://mattvonrocketstein.github.io/heredoc)
- [Jonathan Gardner](http://jonathangardner.net)
- [Julia Evans](https://jvns.ca)
- [Ken Shirriff](http://www.righto.com)
- [Natural Born Coder](http://www.naturalborncoder.com)
- [Rafał Cieślak](https://rafalcieslak.wordpress.com)
- [Robert Strandh](http://metamodular.com)
- [Stratus3D](http://stratus3d.com)
- [Phrack](http://phrack.org)

Operating Systems
- [Alpine Linux](https://alpinelinux.org)
- [Devuan Linux](https://devuan.org)
- [Gentoo Linux](https://gentoo.org)
- [OpenBSD](https://www.openbsd.org)
- [Slackware Linux](http://www.slackware.com)

Mobile
- [LineageOS](https://lineageos.org)
- [postmarketOS](https://postmarketos.org)
- [Replicant](https://replicant.us)
- [SharkBaitOS](https://www.shark-bait.org)

Embedded
- [RetroBSD: Unix for microcontrollers](http://retrobsd.org)
- [FreeRTOS](https://www.freertos.org)

Websites
- [Awk Reference](http://www.grymoire.com/Unix/Awk.html)
- [Barebox](http://www.barebox.org)
- [Buildroot](https://buildroot.uclibc.org)
- [Common Lisp Wiki](http://www.cliki.net)
- [Die.net](https://www.die.net "Die.net")
- [Embedded Linux](http://elinux.org/Main_Page)
- [Free Software Foundation](http://www.fsf.org)
- [Gentoo Cheat Sheet](https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet "Gentoo Cheat Sheet")
- [GNU C Library](https://www.gnu.org/software/libc/manual/html_mono/libc.html)
- [GNU Make](https://www.gnu.org/software/make/manual/make.html)
- [How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html#disclaimer)
- [Internet Engineering Task Force](https://www.ietf.org)
- [Jargon File](http://www.catb.org/~esr/jargon)
- [Lazy Foo Productions](http://lazyfoo.net)
- [Learn X in Y minutes](https://learnxinyminutes.com)
- [LEDE](lede-project.org)
- [Linux From Scratch](http://www.linuxfromscratch.org)
- [Linux Kernel Documentation](https://www.kernel.org/doc/html/latest/index.html)
- [Making Package Friendly Software](http://www.onlamp.com/pub/a/onlamp/2005/03/31/packaging.html)
- [Markdown](https://daringfireball.net/projects/markdown)
- [Openscad Cheatsheet](http://www.openscad.org/cheatsheet/index.html)
- [Operating Systems Development Wiki](http://wiki.osdev.org/Main_Page)
- [Purdue Owl](https://owl.english.purdue.edu/owl)
- [Regexr](http://regexr.com)
- [RFC Index](https://www.rfc-editor.org/rfc-index.html)
- [Rosetta Code](http://rosettacode.org)
- [Semantic Versioning](http://semver.org)
- [ServeTheHome](https://www.servethehome.com)
- [Suckless](http://suckless.org)
- [The Linux Documentation Project](http://tldp.org)
- [Without-Systemd](http://without-systemd.org/wiki/index.php/Main_Page)
- [Worse is Better](https://www.jwz.org/doc/worse-is-better.html)
